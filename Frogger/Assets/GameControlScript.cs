﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControlScript : MonoBehaviour {

    public GameObject hearth1, hearth2, hearth3, GameOver, LvlPassed;
    public static int health = 3;

    void Start()
    {

        hearth1.gameObject.SetActive(true);
        hearth2.gameObject.SetActive(true);
        hearth3.gameObject.SetActive(true);
        GameOver.gameObject.SetActive(false);
    }
    
    void Update()
    {
        switch (health)
        {
            case 3:
                hearth1.gameObject.SetActive(true);
                hearth2.gameObject.SetActive(true);
                hearth3.gameObject.SetActive(true);
                break;
            case 2:
                hearth1.gameObject.SetActive(true);
                hearth2.gameObject.SetActive(true);
                hearth3.gameObject.SetActive(false);
                break;
            case 1:
                hearth1.gameObject.SetActive(true);
                hearth2.gameObject.SetActive(false);
                hearth3.gameObject.SetActive(false);
                break;
            case 0:
                hearth1.gameObject.SetActive(false);
                hearth2.gameObject.SetActive(false);
                hearth3.gameObject.SetActive(false);
                GameOver.gameObject.SetActive(true);
                Time.timeScale = 0f;
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    Score.CurrentScore = 0;
                    Goal.goalsCounter = 0;
                    health = 3;
                    Time.timeScale = 1f;
                    GameOver.gameObject.SetActive(false);
                    SceneManager.LoadScene(1);
                }
                break;
        }

        if(Goal.goalsCounter == 3)
        {
            LvlPassed.gameObject.SetActive(true);
            Time.timeScale = 0f;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if(SceneManager.GetActiveScene().buildIndex == 3)
                {
                    Score.CurrentScore = 0;
                    Goal.goalsCounter = 0;
                    health = 3;
                    Time.timeScale = 1f;
                    SceneManager.LoadScene(1);
                }
                else
                {
                    Time.timeScale = 1f;
                    LvlPassed.gameObject.SetActive(false);
                    Goal.goalsCounter = 0;
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                }
            }
        }
    }
}
