﻿using UnityEngine;

public class LilyScript : MonoBehaviour {

    CircleCollider2D LilyColider;
    SpriteRenderer LilyRenderer;
    float randomLilySpawnDelay = 0f;
	
	void Start () {
        LilyColider = GetComponent<CircleCollider2D>();
        LilyRenderer = GetComponent<SpriteRenderer>();

        LilyDisappears();
    }

    void LilyAppears()
    {
        LilyRenderer.enabled = true;
        LilyColider.enabled = false;
        Invoke("LilyDisappears", 3f);
    }

    void LilyDisappears()
    {
        randomLilySpawnDelay = Random.Range(2f, 6f);
        LilyRenderer.enabled = false;
        LilyColider.enabled = true;
        Invoke("LilyAppears", randomLilySpawnDelay);
    }
}
