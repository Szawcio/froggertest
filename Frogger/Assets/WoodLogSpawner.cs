﻿using UnityEngine;

public class WoodLogSpawner : MonoBehaviour {

    public GameObject woodLog;
    public Transform[] spawPoints;

    float nextTimeToSpawn = 0f;
    public float spawnDeley = .3f;

    void Update()
    {
        if (nextTimeToSpawn <= Time.time)
        {
            SpawnWoodLog();
            nextTimeToSpawn = Time.time + spawnDeley;
        }
    }

    void SpawnWoodLog()
    {
        int randomIndex = Random.Range(0, spawPoints.Length);
        Transform spawnPoint = spawPoints[randomIndex];
        Instantiate(woodLog, spawnPoint.position, spawnPoint.rotation);
    }
}
