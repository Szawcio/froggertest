﻿using UnityEngine;

public class StepPointScript : MonoBehaviour {


    private void OnTriggerEnter2D(Collider2D col)
    {
       if(col.tag == "Frog")
        {
            Score.CurrentScore += 10;
            this.GetComponent<BoxCollider2D>().enabled = false;
        } 
    }
}
