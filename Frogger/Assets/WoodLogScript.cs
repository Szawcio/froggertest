﻿using UnityEngine;

public class WoodLogScript : MonoBehaviour {

    public Rigidbody2D rb;
    BoxCollider2D WoodLogCollider;
    SpriteRenderer WoodLogRenderer;
    Color colorWhenLogRised;
    Color colorWhenLogIsGoingToSink;
    Color colorWhenLogisSunk;
    float position;
    public bool movingRight;

    public float speed = 1f;
    public float randomLogIsGoingToSink = 1f;

    private void Start()
    {
        position = transform.position.x;
        WoodLogCollider = GetComponent<BoxCollider2D>();
        WoodLogRenderer = GetComponent<SpriteRenderer>();
        colorWhenLogRised = new Color(WoodLogRenderer.color.r, WoodLogRenderer.color.g, WoodLogRenderer.color.b, 1f);
        colorWhenLogIsGoingToSink = new Color(WoodLogRenderer.color.r, 0f, WoodLogRenderer.color.b, 1f);
        colorWhenLogisSunk = new Color(WoodLogRenderer.color.r, WoodLogRenderer.color.g, WoodLogRenderer.color.b, 0.5f);

        WoodLogRises();
    }

    void FixedUpdate()
    {
        Vector2 forward = new Vector2(transform.right.x, transform.right.y);
        rb.MovePosition(rb.position + forward * Time.fixedDeltaTime * speed);
        DestroyLogWhenOutSide();

        if(position< transform.position.x)
        {
            movingRight = true;
        }
        else
        {
            movingRight = false;
        }
    }

    private void DestroyLogWhenOutSide()
    {
        if(rb.transform.position.x >= 7f || rb.transform.position.x <= -7f)
        {
            Destroy(gameObject);
        }
    }

    void WoodLogRises()
    {
        randomLogIsGoingToSink = Random.Range(1.5f, 5.5f);
        WoodLogRenderer.color = colorWhenLogRised;
        WoodLogCollider.enabled = true;
        Invoke("WoodIsGoingtoSink", randomLogIsGoingToSink);
    }

    void WoodLogSinks()
    {
        WoodLogRenderer.color = colorWhenLogisSunk;
        WoodLogCollider.enabled = false;
        Invoke("WoodLogRises", 2f);
    }

    void WoodIsGoingtoSink()
    {
        WoodLogRenderer.color = colorWhenLogIsGoingToSink;
        Invoke("WoodLogSinks", 3f);
    }
}
