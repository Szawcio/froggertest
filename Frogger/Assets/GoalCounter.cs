﻿using UnityEngine;
using UnityEngine.UI;

public class GoalCounter : MonoBehaviour {

    public Text goalText;

    void Update()
    {
        goalText.text = ("Goals: " + Goal.goalsCounter.ToString()) +"/3";
    }
}
