﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CountDown : MonoBehaviour {

    public static int timeLeft = 60;
    public Text countdown;
	
	void Start ()
    {
        StartCoroutine("LoseTime");
        Time.timeScale = 1;
	}
	
	void Update ()
    {
        countdown.text = ("Time: " + timeLeft);
	}

    IEnumerator LoseTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            timeLeft--;
            if (timeLeft == 0)
            {
                ResetGameAndTimer();  
            }
    
        }
    }

    public static void ResetGameAndTimer()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        timeLeft = 60;
        GameControlScript.health -= 1;
        
    }
}
