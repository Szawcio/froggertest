﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour {

    public static int goalsCounter = 0;

    private void OnTriggerEnter2D()
    {
        Debug.Log("You WON");
        Score.CurrentScore += 100;
        Score.CurrentScore += CountDown.timeLeft;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        CountDown.timeLeft = 60;
        goalsCounter++;
    }
}
