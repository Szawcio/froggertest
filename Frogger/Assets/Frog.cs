﻿using UnityEngine;

public class Frog : MonoBehaviour {

    public Rigidbody2D rb;
    bool frogOut = true;
    public static bool frogOnLog = false;
    bool frogOnRiver = false;
    float logSpeed;

    Vector2 direction;
    bool frogMovingRightOnLog;

    private void Start()
    {
        frogOnLog = false;
    }

    void Update()
    {
        if (frogOnLog && !CheckIfFrogGoingOutRight(frogOut))
        {
            Vector2 forward = new Vector2(transform.right.x, transform.right.y);
            if (frogMovingRightOnLog)
            {
                rb.MovePosition(rb.position + forward * Time.fixedDeltaTime * logSpeed);
            }
            else if(!frogMovingRightOnLog && !CheckIfFrogGoingOutLeft(frogOut))
            {
                rb.MovePosition(rb.position - forward * Time.fixedDeltaTime * logSpeed);
            } 
        }

        if (Input.GetKeyDown(KeyCode.RightArrow) && !CheckIfFrogGoingOutRight(frogOut))
            direction = Vector2.right;
        else if (Input.GetKeyDown(KeyCode.LeftArrow) && !CheckIfFrogGoingOutLeft(frogOut))
            direction = Vector2.left;
        else if (Input.GetKeyDown(KeyCode.UpArrow))
            direction = Vector2.up;
        else if (Input.GetKeyDown(KeyCode.DownArrow) && !CheckIfFrogGoingOutDown(frogOut))
            direction = Vector2.down;

        if(frogOnRiver&& !frogOnLog)
        {
            CountDown.ResetGameAndTimer();
        }

    }

    private void FixedUpdate()
    {
        if (direction != Vector2.zero)
        {
            rb.MovePosition(rb.position + direction);
            direction = Vector2.zero;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Car")
        {
            Debug.Log("WE LOST!");
            CountDown.ResetGameAndTimer();
        }

        if (col.tag == "Lily")
        {
            Debug.Log("WE LOST!");
            CountDown.ResetGameAndTimer();
        }

        if (col.tag == "WoodLog")
        {
            frogOnLog = true;
            Debug.Log("On Log");
            WoodLogScript log = col.GetComponent<WoodLogScript>();
            logSpeed = log.speed;
            frogMovingRightOnLog = log.movingRight;
        }

        if (col.tag == "River")
        {
            frogOnRiver = true;
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "River")
        {
            frogOnRiver = false;
        }

        if (col.tag == "WoodLog")
        {
           frogOnLog = false;
           Debug.Log("Now, outside of log");
        }
    }

    bool CheckIfFrogGoingOutRight(bool frogOnBoundryRight)
    {
        if(transform.position.x >= 5)
        {
            return frogOnBoundryRight = true;
        }
        return frogOnBoundryRight = false;
    }

    bool CheckIfFrogGoingOutLeft(bool frogOnBoundryLeft)
    {
        if (transform.position.x <= -5)
        {
            return frogOnBoundryLeft = true;
        }
        return frogOnBoundryLeft = false;
    }

    bool CheckIfFrogGoingOutDown(bool frogOnBoundryDown)
    {
        if (transform.position.y == -4.91f || transform.position.y == 7.91f || transform.position.y == 20.73f)
        {
            return frogOnBoundryDown = true;
        }
        return frogOnBoundryDown = false;
    }
}
