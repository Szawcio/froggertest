﻿using UnityEngine;

public class CarSpawner : MonoBehaviour {

    public float spawnDeley = .3f;
    public float nextTimeToSpawn = 0f;

    public GameObject car;
    public Transform[] spawPoints;

     void Update() {
        
        if(nextTimeToSpawn <= Time.time)
        {
            SpawnCar();
            nextTimeToSpawn = Time.time + spawnDeley; 
        } 
     }

    void SpawnCar()
    {
        int randomIndex = Random.Range(0, spawPoints.Length);
        Transform spawnPoint = spawPoints[randomIndex];
        Instantiate(car, spawnPoint.position, spawnPoint.rotation);
    }
}
